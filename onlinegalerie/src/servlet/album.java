package servlet;
import java.io.Serializable;
import java.sql.Date;

public class album implements Serializable{
		private static final long serialVersionUID = 1L;
		
		private Integer idalbum;
		private String albumname;
		private String beschreibung;
		private Date datum;
		private String passwort;
		
	    
		
		public album() {
			super();
		}

		public album(String albumname) {
			this.albumname=albumname;	                
		}
		
		public Integer getidalbum() {
			return idalbum;
		}

		public void setidalbum(Integer idalbum) {
			this.idalbum = idalbum;
		}

		public String getalbumname() {
			return albumname;
		}

		public void setalbumname(String albumname) {
			this.albumname = albumname;
		}
		
		public String getbeschreibung() {
			return beschreibung;
		}
		public void setbeschreibung (String beschreibung) {
			this.beschreibung=beschreibung;
		}
		public Date getdatum() {
			return datum;
		}
		public void setdatum (Date datum) {
			this.datum=datum;
		}
		public String getpasswort() {
			return passwort;
		}
		public void setpasswort(String passwort) {
			this.passwort=passwort;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}
	    
	}

