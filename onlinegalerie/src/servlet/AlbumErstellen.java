package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

/**
 * Servlet implementation class AlbumErstellen
 */
@WebServlet("/AlbumErstellen")
public class AlbumErstellen extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
       
    @Resource(lookup="jdbc/MyTHIPool") 
   	private DataSource ds;

	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try(Connection con = ds.getConnection();
                PreparedStatement pstm = con.prepareStatement("Insert into thidb.album (albumname, beschreibung, datum, passwort) values (?,?,?,?)" )) 
		{
	
			pstm.setString(1, request.getParameter("albumname"));
			pstm.setString(2, request.getParameter("beschreibung"));
			pstm.setDate(3, java.sql.Date.valueOf(java.time.LocalDate.now()));
			pstm.setString(4, request.getParameter("passwort"));
			
			
			pstm.executeUpdate();
			
	    	
		} catch (Exception e) {
			response.getWriter().println(e);
    	}

		 RequestDispatcher view = request.getRequestDispatcher("BildUpload");
	     view.forward(request, response);
	}

	
}


