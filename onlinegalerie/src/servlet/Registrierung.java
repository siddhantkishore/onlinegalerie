package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;


@WebServlet("/Registrierung")
public class Registrierung extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	public Registrierung() {
        super();
      
    }
    
    @Resource(lookup="jdbc/MyTHIPool") 
   	private DataSource ds;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
         
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try(Connection con = ds.getConnection();
                PreparedStatement pstm = con.prepareStatement("Insert into thidb.user (vorname, nachname, benutzername, passwort) values (?,?,?,?)"))
		{
			pstm.setString(1, request.getParameter("vorname"));
			pstm.setString(2, request.getParameter("nachname"));
			pstm.setString(3, request.getParameter("benutzername"));
			pstm.setString(4, request.getParameter("passwort"));
			pstm.executeUpdate();
			
		}
		
		 catch(Exception e){
	            response.getWriter().println(e);
	            
		 }
		 String e = "Neuer Nutzer wurde erfolgreich angelegt.";
	     request.setAttribute("angelegt", e);
		
        RequestDispatcher view = request.getRequestDispatcher("html/anmelden.jsp");
        view.forward(request, response);
        
		}
         

}
