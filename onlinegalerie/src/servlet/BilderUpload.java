package servlet;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.PreparedStatement;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.sql.DataSource;

/**
 * Servlet implementation class BilderUpload
 */

@WebServlet("/BilderUpload")
@MultipartConfig
public class BilderUpload extends HttpServlet {
	private static final long serialVersionUID = 1L;

       
	 @Resource(lookup="jdbc/MyTHIPool") 
	   	private DataSource ds;
	    
    public BilderUpload() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		Part filePart = request.getPart("file");
	    
	    //Find out each part and save in database
	    for (Part part : request.getParts()) {
			if (part != null && part.getSize() > 0) {
				
				//To get fileName
				//String fileName = part.getSubmittedFileName();
				
				//To get content type of file name
				//String contentType = part.getContentType();
				
				InputStream fileContent = filePart.getInputStream();
				
				try(Connection con = ds.getConnection();
						PreparedStatement statement = con.prepareStatement("INSERT INTO thidb.bild.pic VALUES (?)"))
					{
						
						statement.setBlob(1, fileContent);
						
					} catch (Exception e) {
						response.getWriter().println(e);
					}
				
			}
		}
		 RequestDispatcher view = request.getRequestDispatcher("html/profil.jsp");
	     view.forward(request, response);
		
	}

}
