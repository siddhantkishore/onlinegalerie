package servlet;

import java.io.IOException;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import servlet.user;;


@WebServlet("/UserDatenLesen")
public class UserDatenLesen extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   

    @Resource(lookup="jdbc/MyTHIPool") 
   	private DataSource ds;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		ArrayList<user> entries = new ArrayList<>();
	       
		try(Connection con = ds.getConnection();
                PreparedStatement pstm = con.prepareStatement("Select thidb.user.vorname, thidb.user.nachname, thidb.user.benutzername, thidb.user.passwort FROM thidb.user")) 
		{
	
			 try(ResultSet rs = pstm.executeQuery();)
	            {
	                while(rs.next())
	                {
	                	String vorname = rs.getString("vorname");
	                    String nachname = rs.getString("nachname");
	                    String benutzername = rs.getString("benutzername");
	                    String passwort = rs.getString("passwort");
	                    user tmp = new user(vorname, nachname, benutzername, passwort);
	                    entries.add(tmp);
	                }	                    
	             }
	         }
	    	
		catch (Exception e) {
    		throw new ServletException(e.getMessage());
    	}
		 request.setAttribute("Entry", entries);
	        RequestDispatcher view = request.getRequestDispatcher("html/profil.jsp");
	        view.forward(request, response);
	}

	
}

