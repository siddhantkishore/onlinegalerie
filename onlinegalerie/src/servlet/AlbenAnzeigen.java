

package servlet;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import javax.sql.DataSource;


@WebServlet(urlPatterns = {"/AlbenAnzeigen"})
public class AlbenAnzeigen extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
    public AlbenAnzeigen() {
        super();
        // TODO Auto-generated constructor stub
    }

    @Resource(lookup="jdbc/MyTHIPool") 
	private DataSource ds;
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		
		ArrayList<album> entries = new ArrayList<>();
	       
        try(Connection con = ds.getConnection();
            PreparedStatement pstm = con.prepareStatement("Select thidb.album.albumname FROM thidb.album") )
        {
        
          
            try(ResultSet rs = pstm.executeQuery();)
            {
                while(rs.next())
                {
            
               
                    String albumname = rs.getString("albumname");

                                      
                    album tmp = new album(albumname);
                    entries.add(tmp);
                }
                
                    
             }
         }
        
        catch(Exception e)
        {
                response.getWriter().println(e);
        }
       
        
        request.setAttribute("Entry", entries);
        RequestDispatcher view = request.getRequestDispatcher("html/alben.jsp");
        view.forward(request, response);
}
		
	}


