package servlet;
import java.io.Serializable;


public class user implements Serializable{
		private static final long serialVersionUID = 1L;
		
		
		private String vorname;
		private String nachname;
		private String benutzername;
		private String passwort;
		
	    
		
		public user() {
			super();
		}

		public user(String vorname, String nachname, String benutzername, String passwort) {
			super();
			this.vorname = vorname;
			this.nachname=nachname;
			this.benutzername = benutzername;
			this.passwort=passwort;
		}
		
		public String getvorname() {
			return vorname;
		}

		public void setvorname(String vorname) {
			this.vorname = vorname;
		}

		public String getnachname() {
			return nachname;
		}

		public void setnachname(String nachname) {
			this.nachname = nachname;
		}
		
		public String getbenutzername() {
			return benutzername;
		}
		public void setbenutzername (String benutzername) {
			this.benutzername=benutzername;
		}
		
		public String getpasswort() {
			return passwort;
		}
		public void setpasswort(String passwort) {
			this.passwort=passwort;
		}

		public static long getSerialversionuid() {
			return serialVersionUID;
		}
	    
	}

