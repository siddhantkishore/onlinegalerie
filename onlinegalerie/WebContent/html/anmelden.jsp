<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Einloggen</title>
<base href="${pageContext.request.requestURI}"/>
<%@include file="../jspf/header.jspf" %>
<link rel="stylesheet" href="../CSS/maindesign.css">
</head>
<body>
<h1>Anmelden</h1>
<form method="post" action="${pageContext.request.contextPath}/Anmelden">
	<p>E-Mail:<br><input type="text" name="benutzername" form="anmelden"></p>
	<p>Passwort:<br><input type="password" name="passwort" maxlength="20"></p>
	<input type="submit" name="anmelden" value="Login"></input>
</form>
<a href="${pageContext.request.contextPath}/html/registrierung.jsp">Konto erstellen</a>
</body>
<%@include file="../jspf/footer.jspf" %>
</html>