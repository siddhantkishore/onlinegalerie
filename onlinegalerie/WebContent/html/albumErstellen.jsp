<?xml version="1.0" encoding="ISO-8859-1" ?>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@include file="../jspf/header.jspf" %>
<head>
<link rel="stylesheet" href="../CSS/maindesign.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<title>Album erstellen</title>
</head>
<body>
	<h3>Hier k�nnen Sie ein neues Album erstellen</h3>
	
	<form method="post" action="${pageContext.request.contextPath}/AlbumErstellen">
	
			<label><br><input type="text" name="albumname" placeholder="Titel"></label>
			<label><br><input type="text" name="beschreibung" placeholder="Beschreibung"></label>
			<label><br><input type="password" name="passwort" maxlength="20" placeholder="Passwort"></label>
			<label><br><input type="submit" name="erstellen" value="Album erstellen"></label>
		</form>

</body>
<%@include file="../jspf/footer.jspf" %>
</html>